# Example of Jupyter Notebooks

## Configure with envCondaPy3 kernel 

    conda env create -f environment.yml

## Activate

    conda activate envCondaPy3   

## Kernel 

    conda install ipykernel
    python -m ipykernel install --user --name=envCondaPy3

## Extensions

### RISE

    jupyter-nbextension install rise --py --sys-prefix
    jupyter-nbextension enable rise --py --sys-prefix

### nbdime

    jupyter nbextension install --py nbdime --sys-prefix
    jupyter nbextension enable --py nbdime --sys-prefix


